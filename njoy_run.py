import os
import argparse
import shutil

parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-njoyp","--njoypath",default="0",
help="Absolute path to executable njoy21.")
parser.add_argument("-ENDFdir","--ENDFdirectory",default="tungsten_test_ENDFB7.1",
help="Name of the data file for which the probability tables should be prepared.")
parser.add_argument("-T","--temperatures",default=293.15,type=float,nargs="+",
help="Temperature or temperatures for the processing of the neutron data.")
parser.add_argument("-p","--precision",default=0.001,type=float,
help="Precision for the reconstruction of the cross section in NJOY.")
parser.add_argument("-probs","--probabilities",default=20,type=int,
help="Number of generated probabilities for each energy point.")
parser.add_argument("-samp","--samples",default=64,type=int,
help="Number of generated samples of resonance ladders for each energy.")
parser.add_argument("-pigzp","--pigzpath",default="0",
help="Absolute path to executable pigz.")
args=parser.parse_args()

njoypath=args.njoypath
if njoypath=="0":
    print("The path to NJOY was not set.")
    exit()
elif not os.path.exists(os.path.join(njoypath,"njoy21")):
    print('The path to NJOY does not contain executable "njoy21".')
    exit()
datafilename=args.ENDFdirectory
temperatures=args.temperatures
precision=args.precision
probabilities=args.probabilities
samples=args.samples

n_tmp=int(len(temperatures))
tempstring=""
for t in temperatures:
    tempstring+=(" "+str(t))

scriptpath=os.path.abspath(os.path.dirname(__file__))
datapath=os.path.join(scriptpath,datafilename)

if not os.path.exists(datapath):
    print('There is no datapath with the name "'+datafilename+'" in the directory with processing scripts.')
    exit()
elif not os.path.exists(datapath+"/NJOY_run"):
    os.mkdir(datapath+"/NJOY_run")

for fileiterator in os.scandir(datapath):
    if fileiterator.is_file():
        print("Processing "+fileiterator.name+".")
        # read Z and A from ENDF file
        with open(datapath+"/"+fileiterator.name,"r") as file:
            for line in file:
                if "1451" in line:
                    LRP=int(line[30:35].strip())
                    if LRP==0 or LRP==-1:
                        break
                    text=line[:12].strip()
                    if "+" in text:
                        isotopenumber=float(text.split("+")[0])*10**(int(text.split("+")[1]))
                    else:
                        isotopenumber=float(text)
                    Z=int(isotopenumber//1e3)
                    A=int(isotopenumber%1e3)
                    line=file.readline()
                    m=int(line[40:44].strip())
                    code=file.readline()[66:71].strip()
                    break
        # if the ENDF file does not have description of resonances -> continue
        if LRP==0 or LRP==-1:
            print("No description of resonances.")
            continue
        # create workdir and copy ENDF file as tape20
        workdir=os.path.join(datapath,"NJOY_run",str(Z)+"_"+str(A)+"_"+str(m))
        if os.path.exists(os.path.join(workdir)):
            if os.path.exists(os.path.join(workdir,"tape28")):
                continue
            else:
                shutil.rmtree(os.path.join(workdir))
        os.mkdir(workdir)
        shutil.copyfile(datapath+"/"+fileiterator.name,workdir+"/tape20")
        # create content of new input file
        new_file_content=""
        with open(scriptpath+"/njoy21.in","r") as file:
            for line in file:
                if ("PENDF" in line):
                    new_file_content+="'PENDF for "+str(Z)+"_"+str(A)+"_"+str(m)+"'/"+"\n"
                    continue
                if "code" in line:
                    line=line.replace("code",code)
                if "prec" in line:
                    line=line.replace("prec",str(precision))
                if "probs" in line:
                    line=line.replace("probs",str(probabilities))
                if "samp" in line:
                    line=line.replace("samp",str(samples))
                if "ntmp" in line:
                    line=line.replace("ntmp",str(n_tmp))
                if "temp" in line:
                    line=line.replace("temp",tempstring)
                new_file_content+=line
        # create new input file
        with open(workdir+"/njoy21.in","w") as new_file:
            new_file.write(new_file_content)
        # run NJOY
        os.chdir(workdir)
        njoy21=os.path.join(njoypath,"njoy21")
        print("Running NJOY21 in "+workdir+"...")
        os.system(njoy21+" -i njoy21.in -o njoy21.out")

print("JOB DONE!")
