Description:
------------

Set of scripts to generate own probability tables data for use in Geant4 from Evaluated Nuclear Data Files (ENDF) with the use of pre-processing codes NJOY or CALENDF.

Probability tables are mean to describe cross-section in the unresolved resonance region, in which the resonances can be described only by means and distributions of their widths and spacings. These scripts can generate these tables from ENDF files with the use of pre-processing codes NJOY21 and CALENDF and subsequently transform them in the forma usable in Geant4. For more information and to cite use of these scripts please use [1].

[1] M. Zmeškal, L. Thulliez, P. Tamagno, E. Dumonteil, Improvement of Geant4 Neutron-HP package: description of Unresolved Resonance Region with Probability Tables, submited to Annals of Nuclear Energy (2024)

ArXiv: arXiv:2404.16389 (https://doi.org/10.48550/arXiv.2404.16389)


Prerequisites:
--------------
1) Python3 with numpy package.
2) Executable NJOY or CALENDF.
	You can download NJOY here: https://docs.njoy21.io/install.html
	You can download CALENDF here: https://www.oecd-nea.org/tools/abstract/detail/nea-1278/
3) OPTIONAL: pigz software to make the zlib compression of the data for Geant4. If not present, set path to 0 and data will be not compressed.
   Can be installed with: sudo apt-get install pigz
4) Download and unzip this PTGenerator directory.
5) Download the neutron cross sections from desired library into a directory in PTGenerator - you can also create directory there with only the selected isotopes.


1) Set parameters and run:
-----------------------------------------
1.a) For the use of NJOY or CALENDF set the parameters in argumentsNJOY.txt or argumentsCALENDf.txt respectively.
It is important to set the path to executable njoy21 or xcalendf and to set the name of the directory, in which the ENDF files are stored.
For description of the arguments run:
      python3 njoy_run.py -h
   or
      python3 calendf_run.py -h

1.b) Run the creation of probability tables with the pre-processing code with
      python3 njoy_run.py @argumentsNJOY.txt
   or
      python3 calendf_run.py @argumentsCALENDF.txt

After this step, the script will go through all ENDF files in the given directory and check them, if they have data for URR and run the selected pre-processing code to generate PT. All generated files are then stored in $ENDFdirectory/NJOY_run or $ENDFdirectory/CALENDF_run.

2) Generate probability tables for use in Geant4:
-------------------------------------------------
2.a) After running njoy_run.py or calendf_run.py you can run njoy_prepare.py or calendf_prepare.py with the same arguments as:
      python3 njoy_prepare.py @argumentsNJOY.txt
   or 
      python3 calendf_prepare.py @argumentsCALENDF.txt

After this step, the probability tables to be used in Geant4 are generated in directory $ENDFdirectory/G4URRPT/njoy or $ENDFdirectory/G4URRPT/calendf. You can subsequently generate probability tables from both NJOY and CALENDF in one G4URRPT directory.

2.b) For use in Geant4, set the G4URRPTDATA data envivorment variable to the created G4URRPT directory.

2.c) Set the used probability table format in G4ParticleHPManager.hh as USE_PROBABILITY_TABLE_FROM.

