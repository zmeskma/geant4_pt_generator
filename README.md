# Description

   * Set of scripts to generate own probability tables data for use in Geant4 from Evaluated Nuclear Data Files (ENDF) with the use of pre-processing codes NJOY or CALENDF.
   * Probability tables are mean to describe cross-section in the unresolved resonance region, in which the resonances can be described only by means and distributions of their widths and spacings. These scripts can generate these tables from ENDF files with the use of pre-processing codes NJOY21 and CALENDF and subsequently transform them in the format usable in Geant4. For more information and to cite use of these scripts please use [1].


# Contact and citation

## Contact
    Marek Zmeškal
    Faculty of Nuclear Sciences and Physical Engineering CTU in Prague
    Department of Nuclear Reactors
    180 OO Prague (Czech Republic)
    Contact: zmeskma1@fjfi.cvut.cz
    
    Loïc Thulliez
    CEA-Saclay
    DRF/Irfu/DPhN/LEARN
    91191 Gif-sur-Yvette (FRANCE)
    Contact: loic.thulliez@cea.fr

## Citation

If you use this code in anyway in your work please use the reference below since substantial efforts went into developping this code:  

   * M. Zmeškal, L. Thulliez, P. Tamagno, E. Dumonteil, Improvement of Geant4 Neutron-HP package: description of Unresolved Resonance Region with Probability Tables, submited to Annals of Nuclear Energy (2024)
   * ArXiv: arXiv:2404.16389 (https://doi.org/10.48550/arXiv.2404.16389)


# Prerequisites

1) Python3 with numpy package.
2) Executable NJOY or CALENDF. \
	You can download NJOY here: https://docs.njoy21.io/install.html \
	You can download CALENDF here: https://www.oecd-nea.org/tools/abstract/detail/nea-1278/
3) OPTIONAL: pigz software to zlib compress the data for Geant4. If not present, set path to 0 and data will be not compressed. \
   Can be installed with: sudo apt-get install pigz
4) Download and unzip this PTGenerator directory.
5) Download neutron cross sections in ENDF format from desired library - you can also create directory with only selected isotopes.

# Workflow to generate probability tables for Geant4

## Step1: Set parameters and run:

* 1.a) For the use of NJOY or CALENDF set the parameters in argumentsNJOY.txt or argumentsCALENDf.txt respectively.
It is important to set the path to executable njoy21 or xcalendf and to set the name of the directory, in which the ENDF files are stored.\
For description of the arguments run:

      python3 njoy_run.py -h
   or

      python3 calendf_run.py -h

* 1.b) Run the creation of probability tables with the pre-processing code with

      python3 njoy_run.py @argumentsNJOY.txt
   or

      python3 calendf_run.py @argumentsCALENDF.txt

After this step, the script will go through all ENDF files in the given directory and check them, if they have data for URR and run the selected pre-processing code to generate PT. All generated files are then stored in $ENDFdirectory/NJOY_run or $ENDFdirectory/CALENDF_run.

### NJOY settings and parameters
* Cross section reconstruction tolerance is set to 0.1 % (can be changed by user command).
* &sigma;<sub>0</sub> is set to 10<sup>10</sup> for an infinite dilution.
* Number of probability bins is set to 20 (can be changed by user command).
* Number of resonance ladders is set to 64 (can be changed by user command).

More information can be found in NJOY manual:
https://github.com/njoy/NJOY2016-manual/raw/master/njoy16.pdf


### CALENDF settings and parameters
* LCORSCT is set to false (total cross section is the sum of the partial cross sections).
* LFORMRF is set to false (formalism to describe RRR is the one recommended by the evaluator).
* The dense energy grid is taken from example named "inu238e" from CALENDF manual and consist of 11232 energy bins.
* Infinite dilution value is set to 10<sup>10</sup>.
* Precision parameter (ipreci) is set to 6 (can be changed by user command).
* PTs are prepared from .tpc file after regrouping, all negative cross sections are set to 0. 

More information can be found in CALENDF manual:
https://inis.iaea.org/collection/NCLCollectionStore/_Public/43/035/43035075.pdf

## Step2: Generate probability tables for use in Geant4:

* 2.a) After running njoy_run.py or calendf_run.py you can run njoy_prepare.py or calendf_prepare.py with the same arguments as:

      python3 njoy_prepare.py @argumentsNJOY.txt
   or
   
      python3 calendf_prepare.py @argumentsCALENDF.txt

After this step, the probability tables to be used in Geant4 are generated in directory $ENDFdirectory/G4URRPT/njoy or $ENDFdirectory/G4URRPT/calendf. You can subsequently generate probability tables from both NJOY and CALENDF in one G4URRPT directory.

* 2.b) For use in Geant4, set the G4URRPTDATA data envivorment variable to the created G4URRPT directory.

* 2.c) Set the used probability table format in G4ParticleHPManager.hh as USE_PROBABILITY_TABLE_FROM.

