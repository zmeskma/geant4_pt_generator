import os
import argparse
import numpy as np

parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-calendfp","--calendfpath",default="0",
help="Absolute path to executable calendf.")
parser.add_argument("-ENDFdir","--ENDFdirectory",default="tungsten_test_ENDFB7.1",
help="Name of the data file for which the probability tables should be prepared.")
parser.add_argument("-T","--temperatures",default=293.15,type=float,nargs="+",
help="Temperature or temperatures for the processing of the neutron data.")
parser.add_argument("-p","--precision",default=6,type=int,choices=[1,2,3,4,5,6],
help="Precision (ipreci) for the production of probability tables in CALENDF.")
parser.add_argument("-pigzp","--pigzpath",default="0",
help="Absolute path to executable pigz.")
args=parser.parse_args()

datafilename=args.ENDFdirectory
pigzpath=args.pigzpath
if not os.path.exists(os.path.join(pigzpath,"pigz")):
    print('The path to pigz does not contain executable "pigz". The data will be not compressed.' )
    pigzpath="0"

scriptpath=os.path.abspath(os.path.dirname(__file__))
datapath=os.path.join(scriptpath,datafilename)

if not os.path.exists(datapath):
    print('There is no datapath with the name "'+datafilename+'" in the directory with processing scripts.')
    exit()
elif not os.path.exists(datapath+"/CALENDF_run"):
    print("There are no results processed by CALENDF. Run calendfer.py first.")
    exit()
elif not os.path.exists(datapath+"/G4URRPT"):
    os.mkdir(datapath+"/G4URRPT")
if not os.path.exists(datapath+"/G4URRPT/calendf"):
    os.mkdir(datapath+"/G4URRPT/calendf")
ptdatapath=datapath+"/G4URRPT/calendf"

processeddatapath=os.path.join(scriptpath,datafilename,"CALENDF_run")

for fileiterator in os.scandir(processeddatapath):
    if fileiterator.is_file():
        # find all input files and their names
        if ".in" in fileiterator.name:
            splitted_name=fileiterator.name.split("_")
            Z=int(splitted_name[0])
            A=int(splitted_name[1])
            m=int(splitted_name[2].split(".")[0])
            T=int(splitted_name[2].split(".")[1])
            file_name=fileiterator.name.replace("in","tpc")
            if not os.path.exists(os.path.join(processeddatapath,file_name)):
                print("There was a problem preparing data for "+file_name.replace("tpc",""))
                print("Check the output of calendf for this isotope and temperature.")
                continue
            if m==0:
                new_file_name=str(Z)+"_"+str(A)+"."+str(T)+".pt"
            else:
                new_file_name=str(Z)+"_"+str(A)+"_m"+str(m)+"."+str(T)+".pt"
            energies_min=[]
            energies_max=[]
            order=[]
            npar=[]
            theProbabilities=[]
            theTotal=[]
            theElastic=[]
            theCapture=[]
            theFission=[]
            theInelastic=[]
            groups=0
            new_file_content=""
            # read probability tables from tpc file
            with open(processeddatapath+"/"+file_name) as file:
                line=file.readline()
                line=file.readline()
                splitted_line=line.split()
                eminI=splitted_line.index("de")+1
                emaxI=splitted_line.index("a")+1
                groupsI=splitted_line.index("gr.")-1
                emin=float(splitted_line[eminI])
                emax=float(splitted_line[emaxI])
                groups=int(splitted_line[groupsI])
                for line in file:
                    splitted_line=line.split()
                    if "IG" in splitted_line[0]:
                        probabilities=[]
                        total=[]
                        elastic=[]
                        capture=[]
                        fission=[]
                        inelastic=[]
                        partial_list={"prob":[],"tot":[],"2":[],"101":[],"18":[],"4":[],"15":[]}
                        key_list=["prob","tot"]
                        for i in range(int(len(splitted_line))):
                            if "ENG=" in splitted_line[i]:
                                energies_min.append(float(splitted_line[i].strip("ENG=")))
                                energies_max.append(float(splitted_line[i+1]))
                            elif "NOR=" in splitted_line[i]:
                                if splitted_line[i].strip("NOR=")=="":
                                    order.append(int(splitted_line[i+1]))
                                else:
                                    order.append(int(splitted_line[i].strip("NOR=")))
                            elif "NPAR=" in splitted_line[i]:
                                npar.append(int(splitted_line[i].strip("NPAR=")))
                        for par in range(npar[-1]):
                            key_list.append(splitted_line[-5+par])
                        for j in range(order[-1]):
                            line=file.readline()
                            splitted_line=line.split()
                            for s in range(len(splitted_line)):
                                positive=splitted_line[s].split("+")
                                negative=splitted_line[s].split("-")
                                if len(positive)==2:
                                    partial_list[key_list[s]].append(float(positive[0])*10**(int(positive[1])))
                                elif len(negative)==2:
                                    partial_list[key_list[s]].append(float(negative[0])*10**(-1*int(negative[1])))
                                elif len(negative)==3:
                                    partial_list[key_list[s]].append(0)
                                elif (splitted_line[s]=="NaN"):
                                    partial_list[key_list[s]].append(0)
                            for key in (partial_list.keys()-key_list):
                                partial_list[key].append(0)
                        theProbabilities.append(partial_list["prob"])
                        theTotal.append(partial_list["tot"])
                        theElastic.append(partial_list["2"])
                        theCapture.append(partial_list["101"])
                        theFission.append(partial_list["18"])
                        theInelastic.append(partial_list["4"]+partial_list["15"])
            # calculate cumulative probabilities
            for i in range(groups):
                for j in range(order[i]):
                    if j==0:
                        theProbabilities[i][0]=theProbabilities[i][0]
                    elif (j==(order[i]-1)):
                        theProbabilities[i][j]=1.0
                    else:
                        theProbabilities[i][j]=theProbabilities[i][j-1]+theProbabilities[i][j]
            # write new table file
            with open(os.path.join(ptdatapath,new_file_name), 'w') as new_file:
                new_file.write("{:1.6E}".format(energies_min[-1])+" "+"{:1.6E}".format(energies_max[0])+" "+"{:1.6G}".format(groups)+"\n")
                for i in range(groups):
                    new_file.write("{:1.6E}".format(energies_min[-(i+1)])+" "+"{:1.6E}".format(energies_max[-(i+1)])+" "+"{:1.6G}".format(order[-(i+1)])+"\n")
                    for j in range(order[-(i+1)]):
                        new_file.write("{:1.6E}".format(theProbabilities[-(i+1)][j])+" "+"{:1.6E}".format(theTotal[-(i+1)][j])+" "+"{:1.6E}".format(theElastic[-(i+1)][j])+" "+"{:1.6E}".format(theCapture[-(i+1)][j])+" "+"{:1.6E}".format(theFission[-(i+1)][j])+" "+"{:1.6E}".format(theInelastic[-(i+1)][j])+"\n")

if pigzpath!="0":
    pigz=os.path.join(pigzpath,"pigz")
    for fileiterator in os.scandir(ptdatapath):
        if fileiterator.is_file():
            if ".z" in fileiterator.name:
                continue
            else:
                os.chdir(ptdatapath)
                os.system(pigz+' -z1 < "'+fileiterator.name+'" > "'+fileiterator.name+'.z"')
                os.remove(os.path.join(ptdatapath,fileiterator.name))

print("JOB DONE!")
